import imp
import sys
import traceback

try:
    module_required = sys.argv[1]
except IndexError:
    print('main.py: Missing argument')
    exit(1)

try:
    Runner = __import__(module_required + '.function').function.Runner
except ModuleNotFoundError:
    print('main.py: Module "%s" not found!' % module_required)
    exit(2)

try:
    Runner.run()
except Exception as e:
    print('Error running module %s' % module_required)
    traceback.print_exc()
    exit(3)
