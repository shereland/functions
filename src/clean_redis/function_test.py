from datetime import date, timedelta
from clean_redis.function import Runner
import os
import redis
import unittest

class TestCleanRedis(unittest.TestCase):

    def test_delete_correct_key(self):
        """
        Insert data for today, yesterday, two and three days ago
        Just three days ago should be removed
        """
        today = date.today()
        today_key = today.strftime('post_views:%Y%m%d')
        yesterday_key = (today - timedelta(1)).strftime('post_views:%Y%m%d')
        two_days_ago_key = (today - timedelta(2)).strftime('post_views:%Y%m%d')
        three_days_ago_key = (today - timedelta(3)).strftime('post_views:%Y%m%d')

        redis_client = redis.StrictRedis(host=os.environ.get('REDIS_HOST'), port=6379, db=0)

        redis_client.set(today_key, "1")
        redis_client.set(yesterday_key, "1")
        redis_client.set(two_days_ago_key, "1")
        redis_client.set(three_days_ago_key, "1")

        Runner.run()

        assert b"1" == redis_client.get(today_key)
        assert b"1" == redis_client.get(yesterday_key)
        assert b"1" == redis_client.get(two_days_ago_key)
        assert None == redis_client.get(three_days_ago_key)

if __name__ == '__main__':
    unittest.main()
