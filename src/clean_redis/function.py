"""
Currently, it is cleaning old post views from Shereland.
old = more than 3 days ago
"""
from datetime import date, timedelta
import os
import redis


class Runner():

    @classmethod
    def run(self):
        three_days_ago = date.today() - timedelta(3)
        redis_key = three_days_ago.strftime('post_views:%Y%m%d')

        redis_client = redis.StrictRedis(host=os.environ.get('REDIS_HOST',
                                                             'redis_db'),
                                         port=6379, db=0)
        redis_client.delete(redis_key)
