FROM python:3.6-alpine

WORKDIR /code
COPY ["src", "/code"]

RUN pip install -r requirements.txt
CMD ["/bin/sh", "entrypoint.sh"]
